import Vue from "vue";
import Router from "vue-router";
import Login from "./components/layout/Login";
import Dashboard from "./components/layout/Dashboard";
import Projects from "./components/project/ProjectsList";
import Project from "./components/project/Project";
import AddProject from "./components/project/AddProject";
import VueRouter from "vue-router";
Vue.use(Router);

export default new VueRouter({
  mode: "history",
  routes: [
    {
        path: '/',
    },
    {
        path: "/login",
        name: "login",
        component: Login
    },
    {
        path: "/dashboard",
        name: "dashboard",
        component: Dashboard,
        meta: { requiresAuth: true },
        children: [
            {
                path: "projects",
                alias: "projects",
                name: "get_all_projects",
                component: Projects
            },
            {
                path: "projects/:id",
                name: "get_one_project",
                component: Project
            },
            {
                path: "addProject",
                name: "post_project",
                component: AddProject
            }
        ]
    },
  ]
});